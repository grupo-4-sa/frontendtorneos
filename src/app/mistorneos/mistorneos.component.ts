import { TorneoService } from './../_service/torneo.service';
import { JwtService } from './../_service/jwt.service';
import { PartidaService } from './../_service/partida.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Torneo } from './../_model/torneo';
import { ActivatedRoute, Params } from '@angular/router';
import { MistorneosService } from './../_service/mistorneos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JuegoService } from '../_service/juego.service';

@Component({
  selector: 'app-mistorneos',
  templateUrl: './mistorneos.component.html',
  styleUrls: ['./mistorneos.component.css']
})
export class MistorneosComponent implements OnInit {

  torneo: Torneo;
  state: string = "proximos";
  dataSource: MatTableDataSource<Torneo>;
  displayedColumns: string[] = ['nombre', 'juego', 'participantes', 'inscritos', 'estado', 'fecha', 'acciones'];
  displayKeys = true;

  usuario = 0;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private torneoService: TorneoService, private juegoService: JuegoService, private jwtService: JwtService, private misTorneoService: MistorneosService, private partidaService: PartidaService, private route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('id')) {
      this.usuario = parseInt(sessionStorage.getItem('id'));
    }
    this.route.params.subscribe((params: Params) => {
      let param = params['state'];
      this.state = param == null ? this.state : param;
      console.log(this.state);
      this.initForm();
    })

    this.misTorneoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000
      })
    })



  }

  initForm() {
    if (this.state == "activos") {
      this.displayKeys = true;
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.misTorneoService.listarActivos(this.usuario, jwtData.jwt).subscribe(data => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        })
      })
    } else if (this.state == "pasados") {
      this.displayKeys = true;
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.misTorneoService.listarPasados(this.usuario, jwtData.jwt).subscribe(data => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        })
      })
    } else {
      this.displayKeys = false;
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.misTorneoService.listarFuturos(this.usuario, jwtData.jwt).subscribe(data => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        })
      })
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  registrar(torneo: number) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.misTorneoService.registrarATorneo(this.usuario, torneo, jwtData.jwt).subscribe(data => {
        this.misTorneoService.mensajeCambio.next('Se matriculó correctamente al torneo');
        this.initForm();
      })
    })
  }

  validarMovimiento(torneo: number) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.partidaService.intentarJugar(this.usuario, torneo, jwtData.jwt).subscribe(data => {
        if (data.length == 0) {
          alert('Aun no hay una partida disponible');
        } else {
          if (data[0].id_estado == 1) {
            console.log(data[0])
            this.partidaService.simularJuego(data[0], jwtData.jwt).subscribe(datas => {
              this.torneoService.listarPorId(data[0].id_torneo, jwtData.jwt).subscribe(torneoData => {
                this.juegoService.listarPorId(torneoData.id_juego, jwtData.jwt).subscribe(game => {
                  this.partidaService.notificarGeneracion(game.direccion, data[0], jwtData.jwt).subscribe(() => {                    
                    alert(game.front);
                    window.location.href = game.front;
                    this.ngOnInit();
                  })
                })
              })
            })
          } else if (data[0].id_estado == 2) {
            this.torneoService.listarPorId(data[0].id_torneo, jwtData.jwt).subscribe(torneoData => {
              this.juegoService.listarPorId(torneoData.id_juego, jwtData.jwt).subscribe(game => {
                alert(game.front);
                window.location.href = game.front;                  
                this.ngOnInit();
              })
            })
          } else {
            console.log('La partida ya está finalizada');
          }
        }
      })
    })
  }
}
