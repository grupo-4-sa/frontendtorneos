export class CONSTANTS {
    USERS_SERVICE: string;
    TOURNAMENT_BACKEND: string;
    IP_TOKEN: string;
    ID_SECRET: string;
    SECRET_SECRET: string;
    ENCRYPT: string;
}