export class Torneo {
    id_torneo: number;
    nombre: string;
    id_juego: number;
    id_ganador: number;
    id_estado: number;
    inscritos: number;
    fecha: string;
    participantes: number;
    estado: string;
    juego:string;
}