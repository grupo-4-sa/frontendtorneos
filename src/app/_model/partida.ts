export class Partida {
    id_partida: string;
    id_torneo: number;
    id_jugador1: number;
    id_jugador2: number;
    resultado_jugador1: number;
    resultado_jugador2: number;
    id_estado: number;
    llave_siguiente: string;
    ronda: number;
    nombre_jugador1: string
    nombre_jugador2: string
}