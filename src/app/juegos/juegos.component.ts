import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Juego } from './../_model/juego';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JuegoService } from './../_service/juego.service';
import { JwtService } from './../_service/jwt.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {

  
  constructor(private jwtService: JwtService, private juegoService: JuegoService, private snackBar: MatSnackBar) { }
  dataSource: MatTableDataSource<Juego>;
  displayedColumns: string[] = ['nombre', 'juego', 'front', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    this.listar();
    this.juegoService.torneoCambio.subscribe(data => {      
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    })

    this.juegoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000
      })
    })
  }  

  listar() {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.juegoService.listar(jwtData.jwt).subscribe(data => {
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      })
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(id: number) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.juegoService.eliminar(id, jwtData.jwt).subscribe(data => {        
        this.juegoService.listar(jwtData.jwt).subscribe(data => {
          this.juegoService.torneoCambio.next(data)
        });
        this.juegoService.mensajeCambio.next('Se eliminó el torneo');
      })
    })
  }

}
