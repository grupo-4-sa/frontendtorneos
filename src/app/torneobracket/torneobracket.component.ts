import { JwtService } from './../_service/jwt.service';
import { UsuarioService } from './../_service/usuario.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PartidaService } from './../_service/partida.service';
import { Partida } from './../_model/partida';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-torneobracket',
  templateUrl: './torneobracket.component.html',
  styleUrls: ['./torneobracket.component.css']
})
export class TorneobracketComponent implements OnInit {

  
  id:number;    
  finalMatch: Partida = new Partida();
  semiFinal: Partida[];
  Quarter: Partida[];
  roundOf16: Partida[];
  roundOf32: Partida[];
  admin=false;
  

  constructor(private jwtService: JwtService, private partidaService: PartidaService,private usuarioService: UsuarioService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.admin = sessionStorage.getItem('administrador') == "si"? true:false;
    this.route.params.subscribe((params: Params) =>{
      this.semiFinal = []
      this.Quarter = []
      this.roundOf16 = []
      this.roundOf32 = []
      this.id = params['id'];
      this.id = this.id ==null? -1: this.id;     
      this.jwtService.obtenerToken().subscribe(jwtData =>{ 
      this.partidaService.listarPorIdTorneo(this.id, jwtData.jwt).subscribe(data =>{

        console.log(data)
        let sorteData = this.sortArray(data);
        this.finalMatch = sorteData[0]               
        
        console.log(this.finalMatch)
        for (let index = 1; index < sorteData.length; index++) {
          if(index <3){
            this.semiFinal.push(sorteData[index])
          }else if(index < 7){
            this.Quarter.push(sorteData[index])
          }else if(index< 15){
            this.roundOf16.push(sorteData[index])
          }else{
            this.roundOf32.push(sorteData[index])
          }          
        }
      })
    })
    })
    
  }

  sortArray(data: Partida[]):Partida[]{
    let result = []
    for (let index = 1; index <= data.length; index++) {
      for (let control = 0; control < data.length; control++) {
        const element = data[control];
        if(element.ronda == index){      
          if(element.id_jugador1 == -1){
            element.nombre_jugador1 = "Sin definir"
          } else{
            this.jwtService.obtenerToken().subscribe(jwtData =>{
            this.usuarioService.listarPorId(element.id_jugador1, jwtData.jwt).subscribe(data=>{
              if(data != null) element.nombre_jugador1 = data.nombres + data.apellidos; else element.nombre_jugador1 = ""
            })
          })
          }   

          if(element.id_jugador2 == -1){
            element.nombre_jugador2 = "Sin definir"
          } else{
            this.jwtService.obtenerToken().subscribe(jwtData =>{
            this.usuarioService.listarPorId(element.id_jugador2, jwtData.jwt).subscribe(data=>{            
              if(data != null) element.nombre_jugador2 = data.nombres + data.apellidos; else element.nombre_jugador2 = ""
            })
          })
          }             
          result.push(element);
          break;
        }       
      }            
    }
    return result;
  }

  getFinalMatch(data: Partida[]): Partida{
    let element: Partida;
    for (let index = data.length-1; index >= 0; index--) {
       element = data[index];
      if(element.ronda == 1){
        return element;
      }      
    }
  }

  getPreviousGames(siguiente: string, data: Partida[]): Partida[]{
    let resp = []
    let element: Partida;
    for (let index = 0; index < data.length ; index++) {
       element = data[index];
      if(element.llave_siguiente == siguiente){
        resp.push(element)
      }      
    }
    return resp;
  }

}
