import { Globals } from './../_shared/globals';
import { TOURNAMENT_BACKEND } from './../_shared/var.constants';
import { HttpClient } from '@angular/common/http';
import { Torneo } from './../_model/torneo';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MistorneosService {

  torneoCambio = new Subject<Torneo[]>();
  mensajeCambio = new Subject<string>();

  constructor(private global:Globals, private http: HttpClient) {  }

  listarPasados(id: number, jwt:String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Torneo[]>(`${TOURNAMENT_BACKEND}/pasados/${id}`, {headers:headers});
  }

  listarFuturos(id: number, jwt:String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Torneo[]>(`${TOURNAMENT_BACKEND}/futuros/${id}`, {headers:headers});
  }

  listarActivos(id: number, jwt:String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Torneo[]>(`${TOURNAMENT_BACKEND}/activos/${id}`, {headers:headers});
  }

  registrarATorneo(id_usuario: number, id_torneo: number, jwt:String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${TOURNAMENT_BACKEND}/registrar`, {id_usuario: id_usuario, id_torneo:id_torneo}, {headers:headers});
  }

  
}
