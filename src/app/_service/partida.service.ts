import { Globals } from './../_shared/globals';
import { TOURNAMENT_BACKEND } from './../_shared/var.constants';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Partida } from './../_model/partida';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PartidaService {

  torneoCambio = new Subject<Partida[]>();
  mensajeCambio = new Subject<string>();

  constructor( private http: HttpClient) {  }

  listar(jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Partida[]>(`${TOURNAMENT_BACKEND}/partidas`, {headers:headers});
  }

  listarPorIdTorneo(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Partida[]>(`${TOURNAMENT_BACKEND}/partidas/${id}`, {headers:headers});
  }

  jugar(jugador, torneo, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post<Partida[]>(`${TOURNAMENT_BACKEND}/jugar`, {jugador: jugador, torneo:torneo}, {headers:headers});
  }

  intentarJugar(id_usuario: number, id_torneo: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post<Partida[]>(`${TOURNAMENT_BACKEND}/jugar`, {jugador: id_usuario, torneo:id_torneo}, {headers:headers});
  }

  registrarInicioJuego(partida: Partida, jwt:string){
    console.log('Cagadales')
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.put(`${TOURNAMENT_BACKEND}/registrar`, {id: partida.id_partida}, {headers:headers});    
  }

  simularJuego(partida: Partida, jwt:string){
    console.log('Hola Mundo')
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.put(`${TOURNAMENT_BACKEND}/registrar/${partida.id_partida}`,{id:partida.id_partida}, {headers:headers});        
  }

  notificarSimulacion(direccion: String, partida: Partida, jwt:string){
    console.log('Notificar')
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${direccion}/simular`, {id: partida.id_partida, jugadores:[partida.id_jugador1, partida.id_jugador2]}, {headers:headers});        
  }

  notificarGeneracion(direccion: String, partida: Partida, jwt:string){    
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${direccion}/generar`, {id: partida.id_partida, jugadores:[partida.id_jugador1, partida.id_jugador2]}, {headers:headers, responseType: 'text'});        
  }

}
