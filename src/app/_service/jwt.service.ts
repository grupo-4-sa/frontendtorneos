import { Globals } from './../_shared/globals';
import { JWT } from './../_model/usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private global: Globals, private http: HttpClient) { }

  obtenerToken() {

    const headers = { 'Content-Type': 'application/json', 'Authorization': `Basic ${this.global.ENCRYPT}` };
    const body = { title: 'Angular POST Request Example' };
    return this.http.post<JWT>(`${this.global.IP_TOKEN}/token?id=${this.global.ID_SECRET}&secret=${this.global.SECRET_SECRET}`,{}, {headers : headers});
  }


}