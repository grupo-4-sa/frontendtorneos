import { Globals } from './../_shared/globals';
import { TOURNAMENT_BACKEND } from './../_shared/var.constants';
import { Torneo } from '../_model/torneo';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TorneoService {

  torneoCambio = new Subject<Torneo[]>();
  mensajeCambio = new Subject<string>();

  constructor( private http: HttpClient) {  }

  listar( jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Torneo[]>(`${TOURNAMENT_BACKEND}/torneos`, {headers:headers});
  }

  listarPorId(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Torneo>(`${TOURNAMENT_BACKEND}/torneos/${id}`, {headers:headers});
  }

  resgistrar(torneo: Torneo, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${TOURNAMENT_BACKEND}/torneos`, torneo, {headers:headers});
  }

  modificar(torneo: Torneo, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.put(`${TOURNAMENT_BACKEND}/torneos/${torneo.id_torneo}`, torneo, {headers:headers});
  }

  eliminar(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.delete(`${TOURNAMENT_BACKEND}/torneos/${id}`, {headers:headers});
  }

  randomize(id: number, jwt:string ){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${TOURNAMENT_BACKEND}/randomize`, {id: id}, {headers:headers});    
  }

}
