import { TOURNAMENT_BACKEND } from './../_shared/var.constants';
import { Subject } from 'rxjs';
import { Juego } from './../_model/juego';
import { Globals } from './../_shared/globals';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class JuegoService {

  torneoCambio = new Subject<Juego[]>();
  mensajeCambio = new Subject<string>();

  constructor(private global: Globals, private http: HttpClient) {  }

  listar( jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Juego[]>(`${TOURNAMENT_BACKEND}/juegos`, {headers:headers});    
  }

  listarPorId(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Juego>(`${TOURNAMENT_BACKEND}/juegos/${id}`, {headers:headers});
  }

  resgistrar(juego: Juego, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${TOURNAMENT_BACKEND}/juegos`, juego, {headers:headers});
  }

  modificar(juego: Juego, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.put(`${TOURNAMENT_BACKEND}/juegos/${juego.id_juego}`, juego, {headers:headers});
  }

  eliminar(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.delete(`${TOURNAMENT_BACKEND}/juegos/${id}`, {headers:headers});
  }
}
