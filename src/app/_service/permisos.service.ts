import { CONSTANTS } from './../_model/Constants';
import { TOURNAMENT_BACKEND } from './../_shared/var.constants';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Permisos } from './../_model/permisos';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  usuarioCambio = new Subject<Permisos[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http: HttpClient) {  }

  
  listarPorRol(id: number, jwt: String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Permisos[]>(`${TOURNAMENT_BACKEND}/permisos/${id}`, {headers:headers});
  }

  obtenerConstantes(jwt: String){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<CONSTANTS>(`${TOURNAMENT_BACKEND}/variable`, {headers:headers});
  }

}
