import { Globals } from './../_shared/globals';
import { JwtService } from './jwt.service';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Usuario } from './../_model/usuario';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuarioCambio = new Subject<Usuario[]>();
  mensajeCambio = new Subject<string>();

  constructor(private global: Globals ,private http: HttpClient, private jwtService :JwtService) {  }

  listar(jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Usuario[]>(`${this.global.USERS_SERVICE}/jugadores`,{headers:headers});    
    
  }

  listarPorId(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.get<Usuario>(`${this.global.USERS_SERVICE}/jugadores/${id}`,{headers:headers});
  }

  resgistrar(usuario: Usuario, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.post(`${this.global.USERS_SERVICE}/jugadores`, usuario,{headers:headers});
  }

  modificar(usuario: Usuario, jwt:string){    
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.put(`${this.global.USERS_SERVICE}/jugadores/${usuario.id}`, usuario,{headers:headers});
  }

  eliminar(id: number, jwt:string){
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
    return this.http.delete(`${this.global.USERS_SERVICE}/jugadores/${id}`,{headers:headers});
  }

  login(usuario: Usuario, jwt:string){
    console.log(jwt);
    const headers = { 'Content-Type': 'application/json', 'Authorization': `Bearer ${jwt}` };
      return this.http.get<Usuario>(`${this.global.USERS_SERVICE}/login?email=${usuario.email}&password=${usuario.password}`, {headers:headers});    
  }

}
