import { Globals } from './_shared/globals';
import { JwtService } from './_service/jwt.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PermisosService } from './_service/permisos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'torneos';
  permisos = [];
  constructor(private globals: Globals, private jwtService: JwtService, private permisosService: PermisosService, public route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let role = 2;
    if (sessionStorage.getItem('administrador') != null) {
      role = sessionStorage.getItem('administrador') == "si" ? 1 : 0;
    }
    this.jwtService.obtenerToken().subscribe(jwtData => {
      /*
        Se setean las variables al inicio 
      */
     this.permisosService.obtenerConstantes(jwtData.jwt).subscribe(constants =>{
        this.globals.ENCRYPT = constants.ENCRYPT == null? constants.ENCRYPT: this.globals.ENCRYPT;
        this.globals.ID_SECRET = constants.ID_SECRET == null? constants.ID_SECRET: this.globals.ID_SECRET;
        this.globals.IP_TOKEN = constants.IP_TOKEN == null? constants.IP_TOKEN: this.globals.IP_TOKEN;
        this.globals.SECRET_SECRET = constants.SECRET_SECRET == null? constants.SECRET_SECRET: this.globals.SECRET_SECRET;
        this.globals.TOURNAMENT_BACKEND = constants.TOURNAMENT_BACKEND == null? constants.TOURNAMENT_BACKEND: this.globals.TOURNAMENT_BACKEND;
        this.globals.USERS_SERVICE = constants.USERS_SERVICE == null? constants.USERS_SERVICE: this.globals.USERS_SERVICE;
        console.log(constants)
     });


      this.permisosService.listarPorRol(role, jwtData.jwt).subscribe(data => {
        this.permisos = data;
      })
    })

    this.permisosService.mensajeCambio.subscribe(data => {
      let role = 2;
      if (sessionStorage.getItem('administrador') != null) {
        role = sessionStorage.getItem('administrador') == "si" ? 1 : 0;
      }
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.permisosService.listarPorRol(role, jwtData.jwt).subscribe(data => {
          this.permisos = data;
        })
      })
    })
  }

  salir() {
    sessionStorage.clear();
    this.permisosService.mensajeCambio.next('Se realizo un cambio en los permisos');
    this.router.navigate(['/'])
  }
}
