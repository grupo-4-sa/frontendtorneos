import { Juego } from './_model/juego';
import { PartidasComponent } from './partidas/partidas.component';
import { LoginComponent } from './login/login.component';
import { TorneobracketComponent } from './torneobracket/torneobracket.component';
import { UsuarioEdicionComponent } from './pages/usuario-edicion/usuario-edicion.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { MistorneosComponent } from './mistorneos/mistorneos.component';
import { TorneoEdicionComponent } from './pages/torneo/torneo-edicion/torneo-edicion.component';
import { TorneosComponent } from './torneos/torneos.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JuegoEdicionComponent } from './pages/juego-edicion/juego-edicion.component';
import { JuegosComponent } from './juegos/juegos.component';

/*
const routes: Routes = [
  {
    path: 'paciente', component: PacienteComponent, children: [
      { path: 'nuevo', component: PacienteEdicionComponent },
      { path: 'edicion/:id', component: PacienteEdicionComponent }
    ]
  },
  { path: 'medico', component: MedicoComponent }
];
*/
const routes: Routes = [
  {
    path: 'torneo', component: TorneosComponent, children:[
      { path: 'nuevo', component: TorneoEdicionComponent},
      { path: 'edicion/:id', component: TorneoEdicionComponent },   
      { path: 'llaves/:id', component: TorneobracketComponent },
      { path: 'partidas/:id', component: PartidasComponent }      
    ]
  },
  {
    path: 'juego', component: JuegosComponent, children:[
      { path: 'nuevo', component: JuegoEdicionComponent},
      { path: 'edicion/:id', component: JuegoEdicionComponent },           
    ]
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'mistorneos/:state', component: MistorneosComponent, children:[     
      { path: 'llaves/:id', component: TorneobracketComponent }   
    ]
  },
  {
    path: 'usuario', component: UsuariosComponent, children:[
      { path: 'nuevo', component: UsuarioEdicionComponent},
      { path: 'edicion/:id', component: UsuarioEdicionComponent}   
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
