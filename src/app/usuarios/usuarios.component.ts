import { JwtService } from './../_service/jwt.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from './../_model/usuario';
import { UsuarioService } from './../_service/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  constructor(private jwtService: JwtService, private usuarioService: UsuarioService, private snackBar: MatSnackBar) { }
  dataSource: MatTableDataSource<Usuario>;
  displayedColumns: string[] = ['correo', 'nombres', 'apellidos', 'administrador', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    this.listar();
    this.usuarioService.usuarioCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    })

    this.usuarioService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000
      })
    })
  }

  listar() {

    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.usuarioService.listar(jwtData.jwt).subscribe(data => {
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      })
    })

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(id: number) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.usuarioService.eliminar(id, jwtData.jwt).subscribe(data => {
        this.usuarioService.listar(jwtData.jwt).subscribe(data => {
          this.usuarioService.usuarioCambio.next(data)
        });
        this.usuarioService.mensajeCambio.next('Se eliminó el usuario');
      })
    });
  }

}
