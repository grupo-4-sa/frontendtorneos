import { Globals } from './_shared/globals';
import { ServerErrorsInterceptor } from './_shared/server-errors.interceptor';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TorneosComponent } from './torneos/torneos.component';
import { TorneoEdicionComponent } from './pages/torneo/torneo-edicion/torneo-edicion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MistorneosComponent } from './mistorneos/mistorneos.component';
import { TorneobracketComponent } from './torneobracket/torneobracket.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioEdicionComponent } from './pages/usuario-edicion/usuario-edicion.component';
import { LoginComponent } from './login/login.component';
import { PartidasComponent } from './partidas/partidas.component';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoEdicionComponent } from './pages/juego-edicion/juego-edicion.component';

@NgModule({
  declarations: [
    AppComponent,
    TorneosComponent,
    TorneoEdicionComponent,
    MistorneosComponent,
    TorneobracketComponent,
    UsuariosComponent,
    UsuarioEdicionComponent,
    LoginComponent,
    PartidasComponent,
    JuegosComponent,
    JuegoEdicionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [    
    Globals ,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: ServerErrorsInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
