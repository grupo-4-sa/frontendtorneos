import { ActivatedRoute, Router, Params } from '@angular/router';
import { JuegoService } from './../../_service/juego.service';
import { JwtService } from './../../_service/jwt.service';
import { Juego } from './../../_model/juego';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-juego-edicion',
  templateUrl: './juego-edicion.component.html',
  styleUrls: ['./juego-edicion.component.css']
})
export class JuegoEdicionComponent implements OnInit {

  form: FormGroup;
  juego: Juego;
  edicion: boolean;
  id:number;    

  constructor(private jwtService:JwtService, private juegoService: JuegoService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.juego = new Juego();    
    this.form = new FormGroup({
      'id_juego': new FormControl(0),
      'nombre': new FormControl(''),
      'direccion': new FormControl(''),      
      'front': new FormControl(''),      
    });
    this.route.params.subscribe((params: Params) =>{
      this.id = params['id'];
      this.edicion = this.id !=null;      
    })

    this.initForm();
  }

  initForm(){
    if(this.edicion){
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.juegoService.listarPorId(this.id, jwtData.jwt).subscribe(data =>{
        console.log(data)
        this.form = new FormGroup({
          'id_juego': new FormControl(data.id_juego),
          'nombre': new FormControl(data.nombre),
          'direccion': new FormControl(data.direccion),          
          'front': new FormControl(data.front)       
        });
      })
      })
    }
  }

  operar(){
    this.juego.id_juego = this.form.value['id_juego'];
    this.juego.nombre = this.form.value['nombre'];
    this.juego.direccion = this.form.value['direccion'];
    this.juego.front = this.form.value['front'];
    if(this.edicion){  
      console.log(this.juego)    
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.juegoService.modificar(this.juego, jwtData.jwt).subscribe(data =>{
        this.juegoService.listar(jwtData.jwt).subscribe(data =>{
          this.juegoService.torneoCambio.next(data);
          this.juegoService.mensajeCambio.next('Se modificó el juego');
        });
      });
    });
    }else{                  
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.juegoService.resgistrar(this.juego, jwtData.jwt).subscribe(data =>{
        console.log(data)
        this.juegoService.listar(jwtData.jwt).subscribe(data =>{
          this.juegoService.torneoCambio.next(data);
          this.juegoService.mensajeCambio.next('Se creó el juego');
        });
      });
    })
    }

    this.router.navigate(['juego'])
    
    }

}
