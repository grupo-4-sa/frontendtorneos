import { JwtService } from './../../_service/jwt.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Usuario } from './../../_model/usuario';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { UsuarioService } from './../../_service/usuario.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuario-edicion',
  templateUrl: './usuario-edicion.component.html',
  styleUrls: ['./usuario-edicion.component.css']
})
export class UsuarioEdicionComponent implements OnInit {

  
  form: FormGroup;
  usuario: Usuario;
  edicion: boolean;
  id:number;    

  constructor(private jwtService:JwtService, private usuarioService: UsuarioService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.usuario = new Usuario();    
    this.form = new FormGroup({
      'id': new FormControl(0),
      'correo': new FormControl(''),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'password': new FormControl(''),
      'administrador': new FormControl(false)      
    });
    this.route.params.subscribe((params: Params) =>{
      this.id = params['id'];
      this.edicion = this.id !=null;      
    })

    this.initForm();
  }

  initForm(){
    if(this.edicion){
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.usuarioService.listarPorId(this.id, jwtData.jwt).subscribe(data =>{
        console.log(data)
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'correo': new FormControl(data.email),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'password': new FormControl(data.password),
          'administrador': new FormControl(data.administrador)          
        });
      })
      })
    }
  }

  operar(){
    this.usuario.nombres = this.form.value['nombres'];
    this.usuario.apellidos = this.form.value['apellidos'];
    this.usuario.password = this.form.value['password'];      
    this.usuario.administrador = this.form.value['administrador'];      
    this.usuario.email = this.form.value['correo'];      

    if(this.edicion){
      this.usuario.id = this.id
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.usuarioService.modificar(this.usuario, jwtData.jwt).subscribe(data =>{
        this.usuarioService.listar(jwtData.jwt).subscribe(data =>{
          this.usuarioService.usuarioCambio.next(data);
          this.usuarioService.mensajeCambio.next('Se modificó el usuario');
        });
      });
    });
    }else{                  
      this.jwtService.obtenerToken().subscribe(jwtData =>{
      this.usuarioService.resgistrar(this.usuario, jwtData.jwt).subscribe(data =>{
        console.log(data)
        this.usuarioService.listar(jwtData.jwt).subscribe(data =>{
          this.usuarioService.usuarioCambio.next(data);
          this.usuarioService.mensajeCambio.next('Se creó el usuario');
        });
      });
    })
    }

    this.router.navigate(['usuario'])
    
    }

}
