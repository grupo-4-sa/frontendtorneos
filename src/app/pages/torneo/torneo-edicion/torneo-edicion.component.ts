import { Juego } from './../../../_model/juego';
import { JuegoService } from './../../../_service/juego.service';
import { JwtService } from './../../../_service/jwt.service';
import { TorneoService } from './../../../_service/torneo.service';
import { Torneo } from './../../../_model/torneo';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-torneo-edicion',
  templateUrl: './torneo-edicion.component.html',
  styleUrls: ['./torneo-edicion.component.css']
})
export class TorneoEdicionComponent implements OnInit {

  form: FormGroup;
  torneo: Torneo;
  edicion: boolean;
  id:number;  
  games: Juego[] ;
  cants: string[] = ['2', '4', '8', '16', '32']  
  selectedGame;
  selectedCant = this.cants[0];

  constructor(private gamesService: JuegoService,private jwtService: JwtService, private torneoService: TorneoService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.torneo = new Torneo();    
    this.form = new FormGroup({
      'id_torneo': new FormControl(0),
      'nombre': new FormControl(''),
      'id_juego': new FormControl(0),
      'id_ganador': new FormControl(0),
      'id_estado': new FormControl(0),
      'fecha': new FormControl(''),
      'participantes': new FormControl(0),
      'estado': new FormControl('')
    });
    this.route.params.subscribe((params: Params) =>{
      this.id = params['id'];
      this.edicion = this.id !=null;      
    })
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.gamesService.listar(jwtData.jwt).subscribe(data =>{
        this.games = data;
        console.log(data)
        this.selectedGame = this.games[0];
      })
    })
    this.initForm();
  }

  initForm(){
    if(this.edicion){
      this.jwtService.obtenerToken().subscribe(jwtData => {
      this.torneoService.listarPorId(this.id, jwtData.jwt).subscribe(data =>{
        console.log(data)
        this.form = new FormGroup({
          'id_torneo': new FormControl(data.id_torneo),
          'nombre': new FormControl(data.nombre),
          'id_juego': new FormControl(data.id_juego),
          'id_ganador': new FormControl(data.id_ganador),
          'id_estado': new FormControl(data.id_estado),
          'fecha': new FormControl(data.fecha),
          'participantes': new FormControl(data.participantes),
          'estado': new FormControl(data.estado)
        });
      })
      
    })
    }
  }

  operar(){
    

    if(this.edicion){
      this.jwtService.obtenerToken().subscribe(jwtData => {
      this.torneoService.modificar(this.torneo, jwtData.jwt).subscribe(data =>{
        this.torneoService.listar(jwtData.jwt).subscribe(data =>{
          this.torneoService.torneoCambio.next(data);
          this.torneoService.mensajeCambio.next('Se modificó el torneo');
        });
      });
    })
    }else{      
      this.torneo.nombre = this.form.value['nombre'];
      this.torneo.id_juego = this.form.value['id_juego'];
      this.torneo.participantes = this.form.value['participantes'];     
      this.jwtService.obtenerToken().subscribe(jwtData => { 
      this.torneoService.resgistrar(this.torneo, jwtData.jwt).subscribe(data =>{
        this.torneoService.listar(jwtData.jwt).subscribe(data =>{
          this.torneoService.torneoCambio.next(data);
          this.torneoService.mensajeCambio.next('Se creó el torneo');
        });
      });
    })
    }

    this.router.navigate(['torneo'])
    
    }

}
