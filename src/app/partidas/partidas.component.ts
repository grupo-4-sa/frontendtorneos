import { TorneoService } from './../_service/torneo.service';
import { JuegoService } from './../_service/juego.service';
import { JwtService } from './../_service/jwt.service';
import { UsuarioService } from './../_service/usuario.service';
import { Params, ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { Partida } from './../_model/partida';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PartidaService } from './../_service/partida.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-partidas',
  templateUrl: './partidas.component.html',
  styleUrls: ['./partidas.component.css']
})
export class PartidasComponent implements OnInit {


  constructor(private torneoService: TorneoService, private juegoService: JuegoService, private jwtService: JwtService, private partidaService: PartidaService, private usuarioService: UsuarioService, private snackBar: MatSnackBar, private route: ActivatedRoute) { }
  id = -1;
  dataSource: MatTableDataSource<Partida>;
  displayedColumns: string[] = ['id', 'Jugador 1', 'Resultado jugador 1', 'Jugador 2', 'Resultado jugador 2', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.id = this.id == null ? -1 : this.id;
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.partidaService.listarPorIdTorneo(this.id, jwtData.jwt).subscribe(data => {
          data.forEach(element => {
            if (element.id_jugador1 == -1) {
              element.nombre_jugador1 = "Sin definir"
            } else {

              this.usuarioService.listarPorId(element.id_jugador1, jwtData.jwt).subscribe(data => {
                console.log(data)
                if (data != null) element.nombre_jugador1 = data.nombres + data.apellidos; else element.nombre_jugador1 = "";
              })

            }
            if (element.id_jugador2 == -1) {
              element.nombre_jugador2 = "Sin definir"
            } else {
              this.usuarioService.listarPorId(element.id_jugador2, jwtData.jwt).subscribe(data => {
                if (data != null) element.nombre_jugador2 = data.nombres + data.apellidos; else element.nombre_jugador2 = ""
              })

            }


          });
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;

        });
      });

    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  simularPartida(partida: Partida) {
    // Peticion POST para mandar el ID
    console.log(partida)    
    if (partida.id_estado != 2) {      
      this.jwtService.obtenerToken().subscribe(jwtData => {
        this.partidaService.simularJuego(partida, jwtData.jwt).subscribe(data => {          
          this.torneoService.listarPorId(partida.id_torneo, jwtData.jwt).subscribe(torneoData=>{
            this.juegoService.listarPorId(torneoData.id_juego, jwtData.jwt).subscribe(game =>{
              this.partidaService.notificarSimulacion(game.direccion, partida, jwtData.jwt).subscribe(result=>{
                console.log(result)
                alert(game.direccion);
                this.ngOnInit();
              })              
            })
          })
        });
      });
    } else {
      console.log('Partida ya registrada')
    }
    //alert('Saliendo a un sitio externo')
  }


}
