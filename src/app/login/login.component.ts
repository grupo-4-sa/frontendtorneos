import { JwtService } from './../_service/jwt.service';
import { PermisosService } from './../_service/permisos.service';
import { Usuario } from './../_model/usuario';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from './../_service/usuario.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  usuario: Usuario;

  constructor(private jwtService: JwtService, private usuarioService: UsuarioService, private permisosService: PermisosService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.usuario = new Usuario();
    this.form = new FormGroup({
      'correo': new FormControl(''),
      'password': new FormControl(''),
    });

  }


  operar() {
    this.jwtService.obtenerToken().subscribe(data =>{
      console.log(data);
    })
    this.usuario.email = this.form.value['correo'];
    this.usuario.password = this.form.value['password'];
    this.jwtService.obtenerToken().subscribe(data=>{
      this.usuarioService.login(this.usuario, data.jwt).subscribe(data => {
        sessionStorage.setItem('nombres', data.nombres)
        sessionStorage.setItem('apellidos', data.apellidos)
        sessionStorage.setItem('correo', data.email)
        sessionStorage.setItem('id', data.id.toString())
        sessionStorage.setItem('administrador', data.administrador ? "si" : "no")
        if (data.administrador) {
          this.router.navigate(['torneo'])
        } else {
          this.router.navigate(['mistorneos', 'futuros'])
        }
  
        this.permisosService.mensajeCambio.next('Se realizo un cambio en los permisos');
      });
    })
    

  }

}
