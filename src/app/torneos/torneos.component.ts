import { JuegoService } from './../_service/juego.service';
import { JwtService } from './../_service/jwt.service';
import { Torneo } from './../_model/torneo';
import { TorneoService } from './../_service/torneo.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.css']
})
export class TorneosComponent implements OnInit {

  constructor(private jwtService: JwtService, private gamesService: JuegoService, private torneoService: TorneoService, private snackBar: MatSnackBar) { }
  dataSource: MatTableDataSource<Torneo>;
  displayedColumns: string[] = ['nombre', 'juego', 'participantes', 'inscritos', 'estado', 'fecha', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    this.listar();
    this.torneoService.torneoCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    })

    this.torneoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000
      })
    })

    


  }

  organizarTorneo(id) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.torneoService.randomize(id, jwtData.jwt).subscribe(data => {
        this.torneoService.mensajeCambio.next('Ha comenzado el torneo');
        this.listar();
      })
    })
  }

  listar() {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.torneoService.listar(jwtData.jwt).subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      })      
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(id: number) {
    this.jwtService.obtenerToken().subscribe(jwtData => {
      this.torneoService.eliminar(id, jwtData.jwt).subscribe(data => {
        console.log(data)
        this.torneoService.listar(jwtData.jwt).subscribe(data => {
          this.torneoService.torneoCambio.next(data)
        });
        this.torneoService.mensajeCambio.next('Se eliminó el torneo');
      })
    })
  }

}
