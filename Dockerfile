FROM luisferliza/angularmodules:latest as build-step
COPY . .
RUN npm run build --prod

FROM nginx:1.18.0-alpine as prod-stage
COPY --from=build-step /app/dist/torneos /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]